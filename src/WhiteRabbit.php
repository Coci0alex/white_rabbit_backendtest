<?php

class WhiteRabbit
{
    public function findMedianLetterInFile($filePath)
    {
        return ["letter"=>$this->findMedianLetter($this->parseFile($filePath),$occurrences),"count"=>$occurrences];
    }

    /**
     * Parse the input file for letters.
     * @param $filePath
     */
    private function parseFile ($filePath) {
        
        // letters we are looking for //
        $lettersToLookFor = array_fill_keys(range('a', 'z'), 0);
       
        $parsedLetters = array();		
		if ($fp = @fopen($filePath, "r")) {
			
			while(!feof($fp)) {
				$letter = strToLower(fgetc($fp));
				
				// Only count valid letters that occurs in textfile //
				if(array_Key_exists($letter, $lettersToLookFor)) {
					if (array_Key_exists($letter, $parsedLetters)) {
					$parsedLetters[$letter]++;
					} else {
						$parsedLetters[$letter] = 1;
					  }
				}
			}
			fclose($fp);
			//print_r($parsedLetters);
		return $parsedLetters;
		}
		return "File Could Not Be Opened";
	}
	
    /**
     * Return the letter whose occurrences are the median.
     * @param $parsedFile
     * @param $occurrences
     */
    private function findMedianLetter($parsedFile, &$occurrences) {
        asort($parsedFile);
		$letters = array_keys($parsedFile);
		
		if (count($letters) % 2 == 0) {
			$occurrences = $parsedFile[$letters[count($letters)/2 - 1]];
			return $letters[count($letters)/2 - 1];
		} else {
			$occurrences = $parsedFile[$letters[count($letters)/2 + 0.5]];
		    return $letters[count($letters)/2 + 0.5];
	   	  }
	}
}

<?php

class WhiteRabbit2
{
    /**
     * return a php array, that contains the amount of each type of coin, required to fulfill the amount.
     * The returned array should use as few coins as possible.
     * The coins available for use is: 1, 2, 5, 10, 20, 50, 100
     * You can assume that $amount will be an int
     */
    public function findCashPayment($amount){
    
        $keys = array('1','2','5','10','20','50','100');
        $coins = array_fill_keys($keys, 0);        
        $digits = str_split($amount);
        
        // each digits real value can be calculated eg. //
        //  1337 = 1 * 10^3 + 3 * 10^2 + 3 * 10^1 + 7 * 10^0 //
        
		foreach($digits as $i => $item) {
			$exponent = count($digits) - $i - 1;
			$coinToProcess = $item * pow(10, $exponent);
			
			$diff = $coinToProcess;
			
			// Few coins as possible algorithm based on closest coin //
			while($coinToProcess != 0) {
			
			// Increment coin if it appears in coins array // 
				if (array_Key_exists($coinToProcess, $coins)) {
					$coins[$coinToProcess]++;
					$coinToProcess = 0;
				} else {
				
					// compares all coins that are smaller than amount and choose the one with the smallest difference eg. the closest// 
					foreach($keys as $item) {
						if ($coinToProcess > $item) {
							$temp = $coinToProcess - $item;
							if ($temp < $diff) {
								$diff = $temp;
								$coinToPick = $item;
							}
						}	
					}
					$coins[$coinToPick]++;
					$coinToProcess = $coinToProcess - $coinToPick;
					$diff = $coinToProcess;
				}
			}				
		}	
		return $coins;
    }
}

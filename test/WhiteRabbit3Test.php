<?php

namespace test;

require_once(__DIR__ . "/../src/WhiteRabbit3.php");

use PHPUnit\Framework\TestCase;
use WhiteRabbit3;

class WhiteRabbit3Test extends TestCase
{
    /** @var WhiteRabbit3 */
    private $whiteRabbit3;

    public function setUp()
    {
        parent::setUp();
        $this->whiteRabbit3 = new WhiteRabbit3();

    }

    //SECTION FILE !
    /**
     * @dataProvider multiplyProvider
     */
    public function testMultiply($expected, $amount, $multiplier){
        $this->assertEquals($expected, $this->whiteRabbit3->multiplyBy($amount, $multiplier));
    }

	// whenever amount is 7 the test will fail since $guess will always be 0 hence the while loop will never be executed // 
    public function multiplyProvider(){
        return array(
            array(4, 2, 2),
            array(6, 3, 2),
            array(6.25, 2.5, 2.5),
            array(-4, -2, -2),
            array(72, 8, 9),
            array(0, 7, 0),
            array(1, 7, 1),
            array(14, 7, 2),
            array(21, 7, 3),        
            array(700, 7, 100)           
        );
    }
}
